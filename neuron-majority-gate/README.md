# Neuron Simulation

The code neuron.R simulates a formal and a DNA-based CRN of an independent
neuron, as well as a boolean circuit composed of two majority gates and an 
AND gate using the DNA implementation of the chemical neurons as building
blocks. The logic circuit is can be seen in the following figure.

![](circ-diagram.png)

The script also generates the following files:

- `neuron.png`: the simulation of the formal CRN of the neuron in the activated
and deactivated states;
- `neuron-dna.png`: the behavior of the activated DNA-based neuron;
- `circ.png`: the behavior of the complete logic circuit, with the output of
both majority gates and the AND gate.

The output of a neuron is defined by `"A<neuron-name>"`, while the output
of a logic gate is `"A<gate-name>o"`. The following two figures shows 
`neuron.png` and `circ.png`, respectively.

![](neuron.png)

![](circ.png)

The script's output shows data structure that represents the neuron, alongside
simulation metrics, as it can be seen next.


```R
#
# A Neuron
#

Number of reactions : 8

Neuron parameters:
$species
[1] "C_ON"  "X1_ON" "X3_ON" "A_ON"  "B_ON" 

$ci
[1] 1.5 0.0 0.0 1.0 0.0

$reactions
[1] "C_ON -> X1_ON + C_ON" "X1_ON + C_ON -> C_ON" "X1_ON + B_ON -> A_ON" "A_ON -> X1_ON + B_ON" "X3_ON + A_ON -> B_ON" "B_ON -> X3_ON + A_ON" "X3_ON -> 0"          
[8] "0 -> X3_ON"          

$ki
[1]   100     1 50000     1 50000     1     1   100

$name
[1] "_ON"


#
# DNA-based Neuron
#

Number of reactions : 29

#
# Boolean Circuit
#

Number of reactions: 625
Number of species: 975
Number of time points: 1000
Simulation time: 11M 34.7505223751068S
```