# Consensus Simulation

The consensus CRN increases the concentration of the species that starts
with a higher concentration, using the other species as an indirect fuel
for the operation. Its definition is:

```
X + Y -> 2B
B + X -> 2X
B + Y -> 2Y
```

The script `consensus.R` simulates its formal and DNA-based versions, generating
the files:

- `consensus-formal.png`: the plot of the formal CRN simulation;
- `consensus-dna.png`: simulation result of the DNA-based CRN;
- `consensus-combined.png`: the combination of both simulation for comparison.
- `consensus.dna`: a script to be simulated in Microsoft VisualDSD.

The next figure shows the combined result of both simulations.

![](consensus-combined.png)

The script also makes usage of various tools that come with DNAr to help the 
user understand the obtained behavior of the CRN. This tools generate data 
structure that can be printed in the standard output. Think of them like 
debug tools for CRNs.

The script output is:

```R
Behaviour:
        time            X            Y            B
1   0.000000 2.400000e-08 5.600000e-08 0.000000e+00
2   1.666667 1.443554e-08 5.039685e-08 1.516760e-08
3   3.333333 9.496623e-09 5.341322e-08 1.709016e-08
4   5.000000 5.883577e-09 5.921276e-08 1.490366e-08
5   6.666667 3.257287e-09 6.562282e-08 1.111990e-08
6   8.333333 1.596490e-09 7.119578e-08 7.207730e-09
7  10.000000 7.076642e-10 7.515947e-08 4.132867e-09
8  11.666667 2.930128e-10 7.754589e-08 2.161099e-09
9  13.333333 1.130359e-10 7.884135e-08 1.045614e-09
10 15.000000 3.296735e-11 7.952802e-08 4.390102e-10

Y derivatives:
                                                                         Y
1 d[Y]/dt = (-2000 * 2.4e-08[X] * 5.6e-08[Y]) + (2000 * 0[B] * 5.6e-08[Y])

 Derivative values:
(-2000 * 2.4e-08[X] * 5.6e-08[Y])        (2000 * 0[B] * 5.6e-08[Y]) 
                       -2.688e-12                         0.000e+00 

 Derivative result:
[1] -2.688e-12

Comparing with dna results:
           X          Y         B
1 0.05340162 0.08254687 0.1875218
```