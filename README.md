# Supplementary Material of the DNAr paper

Each directory has a description alongside the code of the following examples:

1. [Consensus](/consensus/) (Section 2)
3. [Logic circuit with chemical neurons](/neuron-majority-gate/) (Section 3)
2. [Lotka-Volterra](/lotka-volterra/)