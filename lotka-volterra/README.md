# Lotka-Volterra Simulation

The code `lotka-volterra.R` Simulates the formal CRN of the
Lotka-Volterra oscillator and the DNA-based version. The CRN definition is:
as follows:

```
A + B -> 2B
    A -> 2A
    B -> 0
```

The `A` species starts with a concetration of 2e-8 M and `B` with 1e-8 M.

Both simulations are
combined in the plot named `lotka-volterra.png`, as show in the next figure.

![](lotka-volterra.png)